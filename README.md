## Illyria Dark

![illyria-dark.png](https://i.imgur.com/SQ5vVUk.png)

---

## Illyria Light

![illyria-light.png](https://i.imgur.com/mkgqkaH.png)

---

## groove_grove

This theme is using default terminal background color for backgrounds, which in my case is `#1b2224` / `rgba(27,34,36,1.00)`.

![groove_grove.png](https://i.imgur.com/RBxMFY4.png)